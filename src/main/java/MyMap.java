import java.util.HashMap;
import java.util.Map;

public class MyMap {
    public static void main(String[] args) {

        Map<String, String> languages = new HashMap<>();
        languages.put("JAVA", "compiled. platform independent. loved by many.");
        languages.put("Python", "interpreted high level language.");
        languages.put("GO", "created by Google.");
        languages.put("javascript", "runs on client side.");

        System.out.println(languages.get("JAVA"));

        /*
        * in maps the keys are unique. you can easily override the existing values.
        * if you assign a new value to a key, the old value will be overridden.
        * */
        languages.put("JAVA", "loved by many.");
        System.out.println(languages.get("JAVA"));

        /*
        * put can be used to check of the key is being added for the first time.
        * if yes, it will return null, otherwise it will return the previous value.
        * checkout the lines below.
        * */

        System.out.println(languages.put("JAVA","platform independent."));//prints 'loved by many.'
        System.out.println(languages.get("JAVA"));//prints 'platform independent.'

        /*
        * to check if a key exists in the Map and avoid overriding an existing value you can do this:
        * */
        if(languages.containsKey("JAVA")){
            System.out.println("JAVA is already in the Map.");
        }else{
            //add the key-value pair
        }

        System.out.println("XXXXXXXXXXXXXXXX");

        /*
        * let's print out the contents of our Map
        * observe the output. it is not ordered in the way we put them inside the map or in an alphabetical order.
        * */
        for(String key : languages.keySet()){
            System.out.println(key + " - " + languages.get(key));
        }
    }
}
